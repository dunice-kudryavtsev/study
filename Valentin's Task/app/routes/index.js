mongoose = require("mongoose");
/*
 * GET home page.
 */

exports.index = function(req, res){
  UrlModel = require('../app').UrlModel;
  UrlModel.find(function(err, entries) {
    if (err) return console.error(err);

    var elemOnPage = 10;
    var countPages = entries.length / elemOnPage;
    var paginator = [];
    var pages = [];
    for(var i=0;i<countPages;i++){
      var start = i*elemOnPage;
      var end = start + elemOnPage;
      pages.push( entries.slice(start, end) );
      paginator.push( i+1 );
    }
    console.log(pages);
    res.render('index', {title: 'Get short URL right now! It\'s here only! ', pages: pages, paginator: paginator});
  });
  UrlModel.find({checked: "true"})
};

exports.show_visited = function(req, res){
  UrlModel = require('../app').UrlModel;
  UrlModel.find({checked: 'true'}, function(err, entries) {
    if (err) return console.error(err);

    var elemOnPage = 10;
    var countPages = entries.length / elemOnPage;
    var paginator = [];
    var pages = [];
    for(var i=0;i<countPages;i++){
      var start = i*elemOnPage;
      var end = start + elemOnPage;
      pages.push( entries.slice(start, end) );
      paginator.push( i+1 );
    }
    console.log(pages);
    res.render('index', {title: 'Get short URL right now! It\'s here only! ', pages: pages, paginator: paginator});
  });
};

exports.show_no_visited = function(req, res){
  UrlModel = require('../app').UrlModel;
  UrlModel.find({checked: 'false'}, function(err, entries) {
    if (err) return console.error(err);

    var elemOnPage = 10;
    var countPages = entries.length / elemOnPage;
    var paginator = [];
    var pages = [];
    for(var i=0;i<countPages;i++){
      var start = i*elemOnPage;
      var end = start + elemOnPage;
      pages.push( entries.slice(start, end) );
      paginator.push( i+1 );
    }
    console.log(pages);
    res.render('index', {title: 'Get short URL right now! It\'s here only! ', pages: pages, paginator: paginator});
  });
};

exports.saveUrls = function(req, res, next) {   // Начало SaveUrls
  UrlModel = mongoose.model("url");
  console.log("Get request from ", req.url);
  console.log(req.body);

  newUrl = new UrlModel(req.body);
  newUrl.save(function(err, urlInst) {
    if(err) {
      console.error("Error saving model", err);

      return res.json(500, {
        status : "ERROR",
        modelId: "",
        message: err.message
      });
    }

    if(!urlInst) {
      console.error("No url saved");
      return res.json(500, {
        status : "ERROR",
        modelId: "",
        message: "There aren't the model you are looking for"
      });
    }

    console.log('ADD NEW URL');
    res.status(200).json(urlInst.toJSON());
  });
};

exports.delUrl = function(req, res) {     // Начало delUrl
  UrlModel = mongoose.model("url");
  var elemId = req.body.id;
  var elem = UrlModel.find({_id: elemId});
  elem.remove(function(err, entry) {
    if (err)
      return res.json(500, {
        error: err
      });
    console.log(entry);
     return res.json(200, {});
  });

};

exports.delete_all_elements = function(req, res) {    // Начало dell_all_elements
  UrlModel = mongoose.model("url");
  UrlModel.remove(function (err) {
      if (!err) res.send('200', { error: false, message: '' });
      else res.send('200', {error: true, message: err });
  });
};


exports.delete_selected_elements = function(req, res) {    // Начало delete_selected_elements
  UrlModel = mongoose.model("url");
  var data = req.body.ids;
  console.log('data', data);
  if (!data) return res.send('200', {error : true, message: 'No data'});
  UrlModel.remove({ checked: 'true' }, function (err) {
    console.log('err', err);
    if (!err) return res.send('200', { error: false, message: ''});
    res.send('200', { error: true, message: err });
  });
};

exports.update = function(req, res) {     // Начало Checked true/false
  UrlModel = mongoose.model("url");
  var elemId = req.body.id;
  var newVal = req.body.newVal;
  UrlModel.update({ _id: elemId }, { $set: { checked: newVal }}, function (err) {
    if (err) return res.json(200, { error: err });
    res.json(200, { error: false, message: '' });
  });
};
