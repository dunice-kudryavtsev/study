$(document).ready(function (){
  $('#button').on("click", bitly_url);
  $('.show_long_url').on("click", show_long_url);
  $('#input-field').keyup(function(){
    if(event.keyCode==13)
    {
      bitly_url();
    }
  });
  $('.delete').click(deleteUrl);
  $('#delete_all').on("click", delete_all_elements);
  $('#delete_selected').on("click", delete_selected_elements);
  $('.ch_b').on("click", update);

// Переключение страниц по клику

  $("#content").find("ul:first-child").addClass("active");
  $("#page_navigation").on( "click", "a", function(event){
    var numPage = $(this).data("page");
    $("#content").find("ul").removeClass("active");
    $("#content").find("ul:nth-child("+numPage+")").addClass("active");
  });
}); // Конец ready (function ()

// Присвоение статуса checked элементу с отмеченным чекбоксом

var update = function() {
  var elemId = this.id;
  $.ajax({
    type: "POST",
    url:"http://localhost:3001/update",
    data:{
      id:elemId,
      newVal:this.checked
    },
    dataType:"json",

    error: function (jqXHR, textStatus, errorThrown){
      console.log(textStatus, errorThrown);
    },
    success: function(data, textStatus, jqXHR) {
    }
  })
};

// Показывает скрытый элемент - длинный адрес

var show_long_url = function (e){
  var target = e.target;
  $(target).parent().find(".title").removeClass("title");
};

// Удалние записи

var deleteUrl = function() {
  var elemId = this.id;

  console.log('del this  elem ' + elemId);

  $.ajax({
    type: "POST",
    url:"http://localhost:3001/del",
    data:{id:elemId},
    dataType:"json",

    error: function (jqXHR, textStatus, errorThrown){
      console.log(textStatus, errorThrown);
    },
    success: function(data, textStatus, jqXHR) {
      $("input#"+elemId).parent().parent().remove();
    }

  });
};

// Удаление выбранных записей

var delete_selected_elements = function () {
  var list = $("#list").find('input[type="checkbox"]:checked');

  var ids =[];
  for (var i = 0; i < list.length; i++) {
    ids.push(list[i].id);
  }

  $.ajax({
    type: "POST",
    url:"http://localhost:3001/delSelected",
    data:{ids: ids},
    dataType:"json",

    success: function(data, textStatus, jqXHR) {
      console.log('All components were removed');
      list.each(function (i, el) {
        $(el).parent().parent().remove();
      });
    },
    error: function (jqXHR, textStatus, errorThrown){
      console.log(textStatus, errorThrown);
      console.log('HELLO ERROR!');
    }
  });
};

// Удаление всех записей

var delete_all_elements = function () {
    // Все эл-ты списка $('#list').find("li")
  $.ajax({
    type: "POST",
    url:"http://localhost:3001/delAll",
    dataType: "json",
    success: function(data, textStatus, jqXHR) {
      $('#list').find("ul").empty();
    },
    error: function (jqXHR, textStatus, errorThrown){
      console.log("error", textStatus, errorThrown);
    }
  });
};

// Получение короткого адреса

var bitly_url = function () {
  var url = $('input[name=long-url-text-box]').val();
  var key = ('R_4694cb1c6d9b4d968154923eac579748');
  var username = ('makisimudunice');

  var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
  var urltest=urlRegex.test(url);

  if(!urltest) {
    alert('ERRROR. NO VALID URL!');
    return
  }
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{
      longUrl:url,
      apiKey:key,
      login:username
    },
    dataType:"jsonp",

    success: function (data){
      var short = data.data.url;
      save_url(url, short);
    },

    error: function (status, error){
      console.log(status,error);
    }
  });

  var cleaner = function(){
    $('#input-field').val('');
  };
  cleaner();

};

// Сохранение в БД и вывод

var save_url = function (long_url, short_url) {
  var template = "<li>" +
      "<span class='name'>Short url:" +
        "<a href='PUT_SHORT_URL_HERE'>PUT_SHORT_URL_HERE</a>" +
        "<input type='checkbox' class='ch_b' id='PUT_ID_HERE'></label>" +
        "<br>" +
      "</span>" +
      "<span class='title'>Long url:" +
        "<a href='PUT_LONG_URL_HERE'>PUT_LONG_URL_HERE</a>" +
      "</span>" +
      "<br>" +
      "<input type='button' class='show_long_url' value='Show long URL'>" +
      "<input type='button' class='delete' value='Delete record' id='PUT_ID_HERE'>" +
    "</li>";

  $.ajax({
    url: "/save",
    data: {
      long_url : long_url,
      short_url: short_url,
      checked: false
    },
    type: "POST",
    dataType: "json",

    error: function (jqXHR, textStatus, errorThrown){
      console.log(textStatus, errorThrown);
    },
    success: function(data, textStatus, jqXHR) {
      template = template.replace(/PUT_ID_HERE/g, data._id);
      template = template.replace(/PUT_SHORT_URL_HERE/g, data.short_url);
      template = template.replace(/PUT_LONG_URL_HERE/g, data.long_url);
      $("ul:last-child").append(template);
      $("#list").find("ul").find("li").last().find(".delete").click(deleteUrl);
      $("#list").find("ul").find("li").last().find(".show_long_url").click(show_long_url);
      $("#list").find("ul").find("li").last().find(".ch_b").click(update);
    }

  });
}





