var mongoose = require('mongoose');
// DB connection, forming of data model and saving date of record

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("Connected to mongodb://localhost/Todo");
});
var TodoSchema = new mongoose.Schema({
  text: String,
  Checked: Boolean,
  createdAt: Date,
  updatedAt: Date
});

TodoSchema.pre('save', function(next, done){
  console.log('Pre_save');
  if (this.isNew) {
    this.createdAt = Date.now();
  }
  this.updatedAt = Date.now();
  next();
});

mongoose.model('Todo', TodoSchema);