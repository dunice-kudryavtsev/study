var express = require('express');
var router = express.Router();
var controller = require('../controllers/todos');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'ToDoList' });
});



// Server API Routes
router.param('todoId', controller.todo);

router.post('/api/todos', controller.create);
router.post('/api/todos', controller.remove);

module.exports = router;
