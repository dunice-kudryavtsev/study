var activeTodos = angular.module('tabsControllers', []);

activeTodos.controller("allCtrl", ['$rootScope', '$scope', '$http',
  function ($rootScope, $scope, $http) {
    $rootScope.activeTab = "all";
    $scope.newTodo = $rootScope.newTodo;
    console.log("all", new Date().getTime());

    $rootScope.add_todo = function() {
      $scope.todos.push($scope.newTodo);
      $scope.newTodo = '';
    };
  }]);

activeTodos.controller("activeCtrl", ['$rootScope', '$scope', '$http',
  function ($rootScope, $scope, $http) {
    $rootScope.activeTab = "active";
    $scope.newTodo = $rootScope.newTodo;
    $
    console.log("active", new Date().getTime());

    $rootScope.add_todo = function() {
      $scope.todos.push($scope.newTodo);
      $scope.newTodo = '';
    };
  }]);

activeTodos.controller("completedCtrl", ['$rootScope', '$scope', '$http',
  function ($rootScope, $scope, $http) {
    $rootScope.activeTab = "completed";
    $scope.newTodo = $rootScope.newTodo;
    console.log("completed", new Date().getTime());

    $rootScope.add_todo = function() {
      $scope.todos.push($scope.newTodo);
      $scope.newTodo = '';

    };
  }]);

