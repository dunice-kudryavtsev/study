var mean = angular.module('MEAN', [
  'ngResource',
  'ngRoute',
  "tabsControllers"
]);


mean.controller('MainCtrl', ['$rootScope', '$scope', function ($rootScope, $scope, id) {
  $rootScope.activeTab = "/";
    console.log("Есть контакт!!!");
    $scope.todos = [
      {id: "id", todo: "todo", checked: true, title: "1"},
      {id: "id", todo: "todo", title: "2"},
    ];

    $scope.redirect = function(route){
      window.location.href='#/' + route
    };
  //
  //$scope.removeTodo = function (id) {
  //  $scope.todos[id].$remove();
  //  $scope.todos.splice(id, 1);
  //};
  }]);


mean.config(function($routeProvider, $locationProvider){
    $routeProvider.when('/', {
        templateUrl: "/partials/main-template.html",
        controller: "MainCtrl"
      }).
      when('/all', {
        templateUrl: "/partials/tab-all.html",
        controller: "allCtrl"
      }).
      when('/active', {
        templateUrl: "/partials/tab-active.html",
        controller: "activeCtrl"
      }).
      when('/completed', {
        templateUrl: "/partials/tab-completed.html",
        controller: "completedCtrl"
      });

    $routeProvider.otherwise({
        redirectTo: '/'
      });
  });

mean.run(["$rootScope", function($rootScope) {
  $rootScope.activeTab = "";
  $rootScope.newTodo = "";
  console.log("run", new Date().getTime());
}]);