$( document ).ready(function() {
    console.log( "I'm ready, commander!" );

     var post_template = _.template(
       $( "script#post-template" ).html()
     );

     // Data arrays

     var titles = [];
     titles[0]="First random title. You are lucky!";
     titles[1]="Second random title. So great!";
     titles[2]="Third random title. Wow!";
     titles[3]="Fourth random title. I'm impressed!";

     var posts = [];
     posts[0]="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et \
     dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \
     commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla \
     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
     posts[1]="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore \
     magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ";
     posts[2]="Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur \
     sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
     posts[3]="Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \
     Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.";

     var user_names = [];
     user_names[0]="The Guardian";
     user_names[1]="The New York Times";
     user_names[2]="Time";
     user_names[3]="Mike Williams";

     var video_imgs = [];
     video_imgs[0]="public/images/youtube_maket.jpg";
     video_imgs[1]="public/images/video_maket.jpg";

     var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];


     // Functions

     var random_function = function(min, max){
       return Math.floor(Math.random() * (max - min + 1)) + min;
     };

  var PostsCount = 10,
    futurePosts = [],
    allTypes = ["text", "audio", "video", "image", "twit"];


  _.each(_.range(PostsCount), function(el, i) {
    var type, title, text, image, audio, video, twit, author, date, location, rating, commentCount, stringTime,
      templateContext = {};
    type = allTypes[random_function(0, allTypes.length-1)];
    templateContext.type = type;

    title = titles[random_function(0, titles.length-1)];
    templateContext.title = title;

    switch (type) {
      case "text": text = posts[random_function(0, posts.length-1)]; break;
      case "audio": audio = "public/images/music_player_maket.jpg"; break;
      case "video": video = video_imgs[random_function(0, video_imgs.length-1)]; break;
      case "image": image = "public/images/cosmonaut.jpg"; break;
      case "twit": twit = "public/images/tvitter_maket.jpg"; break;
      default : text = "Wrong type"; templateContext.type = "text";
    }
    templateContext.text = text;
    templateContext.audio = audio;
    templateContext.video = video;
    templateContext.image = image;
    templateContext.twit = twit;

    author = user_names[random_function(0, user_names.length-1)];
    templateContext.author = author;

    date = new Date(Math.random()*1000000000000);

    var normalMonth = monthNames[date.getMonth()]; // Месяц в сокращенном текстовом формате
    var normalDay = date.getDate();  // День месяца
    var normalHours = date.getHours();  // Часы
    var normalMinutes = date.getMinutes();  // Минуты
    var hours, amPm; // Переменные для составления правильного формата времени
    var hoursFormating = function(){
      if (normalHours-12 <= 0 ){
        hours = normalHours;
        amPm = "am";
      } else {
        hours = normalHours - 12;
        amPm = "pm";
      }
    };
    hoursFormating();

    stringTime = normalMonth + ' ' + normalDay + ', ' + hours + " : " + normalMinutes + ' ' + amPm; // Итоговый вывод даты в нужном формате

    templateContext.date = stringTime;

    location = "Location coming soon";
    templateContext.location = location;

    rating = random_function(0, 50);
    templateContext.rating = rating;

    commentCount = random_function(0, 15);
    templateContext.commentCount = commentCount;

    futurePosts.push(post_template(templateContext));
  });

  $("div.audioPartTemplate").css("background-image","url(public/images/music_player_maket.jpg)");

  $("main#page_content").append(futurePosts)
});